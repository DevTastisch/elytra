---- Minecraft Crash Report ----
// Shall we play a game?

Time: 03.07.18 19:31
Description: Exception ticking world

java.lang.NullPointerException: Exception ticking world
	at org.elytra.impl.world.ElytraChunk.execute(ElytraChunk.java:51)
	at net.minecraft.server.management.PlayerManager$PlayerInstance.onUpdate(PlayerManager.java:436)
	at net.minecraft.server.management.PlayerManager.updatePlayerInstances(PlayerManager.java:78)
	at net.minecraft.world.WorldServer.tick(WorldServer.java:216)
	at net.minecraft.server.MinecraftServer.updateTimeLightAndEntities(MinecraftServer.java:757)
	at net.minecraft.server.dedicated.DedicatedServer.updateTimeLightAndEntities(DedicatedServer.java:356)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:678)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:557)
	at java.lang.Thread.run(Thread.java:748)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Stacktrace:
	at org.elytra.impl.world.ElytraChunk.execute(ElytraChunk.java:51)
	at net.minecraft.server.management.PlayerManager$PlayerInstance.onUpdate(PlayerManager.java:436)
	at net.minecraft.server.management.PlayerManager.updatePlayerInstances(PlayerManager.java:78)
	at net.minecraft.world.WorldServer.tick(WorldServer.java:216)

-- Affected level --
Details:
	Level name: world
	All players: 1 total; [EntityPlayerMP['DevTastisch'/194, l='world', x=-143,71, y=119,49, z=311,91]]
	Chunk stats: ServerChunkCache: 667 Drop: 0
	Level seed: 4039130249284089887
	Level generator: ID 00 - default, ver 1. Features enabled: true
	Level generator options: 
	Level spawn location: -152,00,64,00,252,00 - World: (-152,64,252), Chunk: (at 8,4,12 in -10,15; contains blocks -160,0,240 to -145,255,255), Region: (-1,0; contains chunks -32,0 to -1,31, blocks -512,0,0 to -1,255,511)
	Level time: 2458 game time, 2458 day time
	Level dimension: 0
	Level storage version: 0x04ABD - Anvil
	Level weather: Rain time: 79119 (now: false), thunder time: 97935 (now: false)
	Level game mode: Game mode: survival (ID 0). Hardcore: false. Cheats: false
Stacktrace:
	at net.minecraft.server.MinecraftServer.updateTimeLightAndEntities(MinecraftServer.java:757)
	at net.minecraft.server.dedicated.DedicatedServer.updateTimeLightAndEntities(DedicatedServer.java:356)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:678)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:557)
	at java.lang.Thread.run(Thread.java:748)

-- System Details --
Details:
	Minecraft Version: 1.8.9
	Operating System: Windows 8.1 (amd64) version 6.3
	Java Version: 1.8.0_151, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 178363488 bytes (170 MB) / 375914496 bytes (358 MB) up to 2356674560 bytes (2247 MB)
	JVM Flags: 0 total; 
	IntCache: cache: 0, tcache: 0, allocated: 12, tallocated: 94
	Profiler Position: N/A (disabled)
	Player Count: 1 / 20; [EntityPlayerMP['DevTastisch'/194, l='world', x=-143,71, y=119,49, z=311,91]]
	Is Modded: Unknown (can't tell)
	Type: Dedicated Server (map_server.txt)
package org.elytra.impl.packetlistener;

import net.minecraft.network.EnumPacketDirection;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import org.elytra.api.player.Player;
import org.elytra.impl.packet.PacketListenerInitializer;

public class C08PacketPlayerBlockPlacementListener extends PacketListenerInitializer<C08PacketPlayerBlockPlacement> {

    public C08PacketPlayerBlockPlacementListener(){
        super(EnumPacketDirection.SERVERBOUND);
    }

    public void handle(Player player, C08PacketPlayerBlockPlacement packet) {
    }
}

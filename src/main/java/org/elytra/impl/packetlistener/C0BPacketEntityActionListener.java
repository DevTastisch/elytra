package org.elytra.impl.packetlistener;

import net.minecraft.network.EnumPacketDirection;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import org.elytra.Elytra;
import org.elytra.api.player.Player;
import org.elytra.impl.event.player.AsyncPlayerToggleSneakEvent;
import org.elytra.impl.event.player.AsyncPlayerToggleSprintEvent;
import org.elytra.impl.packet.PacketListenerInitializer;

public class C0BPacketEntityActionListener extends PacketListenerInitializer<C0BPacketEntityAction> {

    public C0BPacketEntityActionListener(){
        super(EnumPacketDirection.SERVERBOUND);
    }

    public void handle(Player player, C0BPacketEntityAction packet) {
        switch (packet.getAction()){
            case START_SNEAKING:
                Elytra.getServer().callAsyncEvent(new AsyncPlayerToggleSneakEvent(player, true));
                break;

            case STOP_SNEAKING:
                Elytra.getServer().callAsyncEvent(new AsyncPlayerToggleSneakEvent(player, false));
                break;

            case START_SPRINTING:
                Elytra.getServer().callAsyncEvent(new AsyncPlayerToggleSprintEvent(player, true));
                break;

            case STOP_SPRINTING:
                Elytra.getServer().callAsyncEvent(new AsyncPlayerToggleSprintEvent(player, false));
                break;
        }
    }
}

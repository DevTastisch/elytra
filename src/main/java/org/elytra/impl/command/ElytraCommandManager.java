package org.elytra.impl.command;

import net.minecraft.command.ServerCommandManager;
import org.elytra.Main;
import org.elytra.api.command.Command;
import org.elytra.api.property.command.CommandName;
import org.elytra.api.property.command.CommandPermission;
import org.elytra.impl.plugin.AnnotationFinder;
import org.elytra.impl.plugin.ElytraPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ElytraCommandManager extends ServerCommandManager {

    public ElytraCommandManager() {
        super();
        loadCommands();
    }

    public void loadCommands() {
        List<ClassLoader> classloaders = new ArrayList<>();
        classloaders.add(Main.class.getClassLoader());
        ElytraPlugin.getPlugins().forEach(elytraPlugin -> {
            classloaders.add(elytraPlugin.getJarClassLoader());
        });

        AtomicReference<List<String>> classNameCache = new AtomicReference<>(new ArrayList<>());

        classloaders.forEach(classLoader -> {
            new AnnotationFinder(classNameCache.get(), classLoader, CommandName.class) {
                public void handleClass(Class clazz) {
                    if (Arrays.stream(clazz.getInterfaces()).anyMatch(iclazz -> iclazz.equals(Command.class))) {
                        try {
                            Command command = (Command) clazz.newInstance();
                            CommandName name = (CommandName) clazz.getAnnotation(CommandName.class);
                            CommandPermission permission = clazz.isAnnotationPresent(CommandPermission.class) ? (CommandPermission) clazz.getAnnotation(CommandPermission.class) : null;
                            registerCommand(new ElytraCommandBuilder(command, name.value().toLowerCase()).setPermission(permission != null ? permission.value() : null).build());

                        } catch (InstantiationException | IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
        });
    }


}
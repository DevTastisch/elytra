package org.elytra.impl.command;

import lombok.Data;
import lombok.experimental.Accessors;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.BlockPos;
import org.elytra.Elytra;
import org.elytra.api.command.Command;
import org.elytra.api.command.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Accessors(chain = true)
@Data
public class ElytraCommandBuilder {

    private final Command command;
    private final String commandName;
    private String permission;
    private String commandUsage = "Unknown Command";
    private String[] alias = new String[]{};
    private ProcessCommand processCommand;
    private CanCommandSenderUseCommand canCommandSenderUseCommand;
    private AddTabCompletionOptions addTabCompletionOptions;
    private IsUsernameIndex isUsernameIndex;
    private CompareTo compareTo;


    public ElytraCommandBuilder(Command command, String commandName) {
        this.command = command;
        this.commandName = commandName;

        this.processCommand = (sender, args) -> {
            if(sender instanceof EntityPlayerMP){
                this.command.exceute(Elytra.getServer().getPlayer(((EntityPlayerMP) sender).getUniqueID()), args);
            }
        };
        this.canCommandSenderUseCommand = (iCommandSender) -> true;
        this.addTabCompletionOptions = (iCommandSender, args, pos) -> new ArrayList<>();
        this.isUsernameIndex = (args, index) -> true;
        this.compareTo = (o) -> commandName.compareTo(o.getCommandName());
    }


    public ElytraCommand build() {
        return new ElytraCommand() {
            public String getCommandName() {
                return commandName;
            }

            public String getCommandUsage(ICommandSender sender) {
                return commandUsage;
            }

            public List<String> getCommandAliases() {
                return Arrays.asList(alias);
            }

            public void processCommand(ICommandSender sender, String[] args) throws CommandException {
                processCommand.processCommand(sender, args);
            }

            public boolean canCommandSenderUseCommand(ICommandSender sender) {
                return canCommandSenderUseCommand.canCommandSenderUseCommand(sender);
            }

            public List<String> addTabCompletionOptions(ICommandSender sender, String[] args, BlockPos pos) {
                return addTabCompletionOptions.addTabCompletionOptions(sender, args, pos);
            }

            public boolean isUsernameIndex(String[] args, int index) {
                return isUsernameIndex.isUsernameIndex(args, index);
            }

            public int compareTo(ICommand o) {
                return compareTo.compareTo(o);
            }

            public String getPermission() {
                return permission;
            }
        };
    }
}

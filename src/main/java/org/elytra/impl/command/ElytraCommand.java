package org.elytra.impl.command;

import net.minecraft.command.ICommand;

public abstract class ElytraCommand implements ICommand {

    public abstract String getPermission();

}

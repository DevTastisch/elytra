package org.elytra.impl.command.defaultcommand;

import lombok.Builder;
import lombok.Singular;
import lombok.extern.java.Log;
import org.elytra.Elytra;
import org.elytra.api.command.Command;
import org.elytra.api.permission.PermissionGroup;
import org.elytra.api.player.Player;
import org.elytra.impl.ElytraServer;
import org.elytra.impl.permission.PermissionManager;

public class PermissionCommand implements Command {

    public void exceute(Player player, String[] args) {


        PermissionManager permissionManager = ((ElytraServer) Elytra.getServer()).getPermissionManager();
        switch (args.length) {
            case 1:
                switch (args[0].toLowerCase()) {
                    case "groups":
                        permissionManager.getPermissionGroups().forEach(permissionGroup -> player.sendMessage(" §7- " + permissionGroup.getName()));
                        break;
                }
                break;

            case 3:
                switch (args[0].toLowerCase()) {
                    case "group":
                        PermissionGroup group = permissionManager.getGroup(args[1]);
                        if (group != null) {
                            switch (args[2]) {
                                case "list":
                                    group.getPermissions().forEach(permission -> player.sendMessage(" §7" + permission));
                                    break;
                            }
                        } else {
                            //TODO
                        }
                }

            case 4:
                switch (args[0].toLowerCase()) {
                    case "group":
                        PermissionGroup group = permissionManager.getGroup(args[1]);
                        if (group != null) {
                            switch (args[2]) {
                                case "add":
                                    group.addPermission(args[3]);
                                    break;
                                case "remove":
                                    group.removePermission(args[3]);
                                    break;
                            }
                        } else {
                            //TODO
                        }

                }

        }
    }
}

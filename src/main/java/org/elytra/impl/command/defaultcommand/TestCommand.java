package org.elytra.impl.command.defaultcommand;

import org.elytra.api.command.Command;
import org.elytra.api.player.Player;
import org.elytra.api.property.command.CommandAlias;
import org.elytra.api.property.command.CommandNoPermission;
import org.elytra.api.property.command.CommandPermission;
import org.elytra.api.property.general.ConfigLink;
import org.elytra.api.property.command.CommandName;
import org.elytra.api.scoreboard.handler.ScoreboardSidebarHandler;
import org.elytra.impl.scoreboard.DisplaySlot;

import java.util.Arrays;
import java.util.List;

@CommandNoPermission("Du hast keine Rechte")
@CommandName("TestCommand")
public class TestCommand implements Command {

    @ConfigLink(key = "TestField")
    private String test;


    public void exceute(Player player, String[] args) {

        @ConfigLink
        String hanselStinkt = "test.config.value";

        System.out.println(Integer.toBinaryString(System.identityHashCode(hanselStinkt)));

        System.out.println(hanselStinkt);
        player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setScoreboardHandler(new ScoreboardSidebarHandler() {
            public String getTitle() {
                return "§cDemo";
            }

            public List<String> getLines() {
                return Arrays.asList(
                        "§4Mehlwin",
                        "§cist",
                        "§7ein",
                        "§aStinka!"
                );
            }
        });
        player.updateScoreboard();
    }

}

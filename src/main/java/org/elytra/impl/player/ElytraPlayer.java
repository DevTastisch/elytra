package org.elytra.impl.player;

import lombok.Data;
import lombok.experimental.Accessors;
import net.minecraft.command.CommandResultStats;
import net.minecraft.command.ICommand;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.EnumPacketDirection;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S02PacketChat;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.Vec3;
import org.elytra.Elytra;
import org.elytra.api.permission.PermissionGroup;
import org.elytra.api.player.Player;
import org.elytra.api.scoreboard.Scoreboard;
import org.elytra.api.world.World;
import org.elytra.impl.ElytraServer;
import org.elytra.impl.command.ElytraCommand;
import org.elytra.impl.world.Location;
import org.elytra.impl.world.ElytraWorld;

import java.util.List;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

@Accessors(chain = true)
@Data
public class ElytraPlayer implements Player {

    private final EntityPlayer entityPlayer;

    public ElytraPlayer(EntityPlayer entityPlayer) {
        this.entityPlayer = entityPlayer;
    }


    public Location getLocation() {
        return new Location(this.getWorld(), this.entityPlayer.posX, this.entityPlayer.posY, this.entityPlayer.posZ, this.entityPlayer.rotationYaw, this.entityPlayer.rotationPitch);
    }

    public boolean isSneaking() {
        return this.entityPlayer.isSneaking();
    }

    public boolean isSprinting() {
        return this.entityPlayer.isSprinting();
    }

    public Player sendPacket(Packet<?> packet) {
        this.getMP().playerNetServerHandler.sendPacket(packet);
        return this;
    }

    public Player sendMessage(String message) {
        this.sendMessage(new ChatComponentText(message));
        return this;
    }

    public Player sendMessage(IChatComponent message) {
        this.getMP().playerNetServerHandler.getNetworkManager().sendPacket(new S02PacketChat(message));
        return this;
    }

    public <T extends Packet<?>> Player addIncomingPacketListener(BiConsumer<Player, T> consumer) {
        this.getMP().playerNetServerHandler.getNetworkManager().registerPacketListener(((Consumer<T>) (packet) -> consumer.accept(this, packet)), EnumPacketDirection.SERVERBOUND);
        return this;
    }

    public <T extends Packet<?>> Player addOutgoingPacketListener(BiConsumer<Player, T> consumer) {
        this.getMP().playerNetServerHandler.getNetworkManager().registerPacketListener(((Consumer<T>) (packet) -> consumer.accept(this, packet)), EnumPacketDirection.CLIENTBOUND);
        return this;
    }

    public EntityPlayerMP getMP() {
        return ((EntityPlayerMP) this.entityPlayer);
    }

    public World getWorld() {
        return this.entityPlayer.getEntityWorld().getNethWorld();
    }

    public UUID getUUID() {
        return this.entityPlayer.getGameProfile().getId();
    }

    public String getName() {
        return this.getEntityPlayer().getGameProfile().getName();
    }

    public IChatComponent getDisplayName() {
        return this.getEntityPlayer().getDisplayName();
    }

    public void addChatMessage(IChatComponent component) {
        this.sendMessage(component);
    }

    public boolean canCommandSenderUseCommand(int permLevel, String commandName) {
        ICommand command = ((ElytraServer) Elytra.getServer()).getMinecraftServer().getCommandManager().getCommand(commandName);
        if(command instanceof ElytraCommand){
            ElytraCommand nethitCommand = ((ElytraCommand) command);
            return hasPermission(nethitCommand.getPermission());
        }
        return true;
    }

    public BlockPos getPosition() {
        return null;
    }

    @Override
    public Vec3 getPositionVector() {
        return null;
    }

    public net.minecraft.world.World getEntityWorld() {
        return ((ElytraWorld) this.getWorld()).getNmsWorld();
    }

    public Entity getCommandSenderEntity() {
        return getEntityPlayer();
    }

    public boolean sendCommandFeedback() {
        return true;
    }

    public void setCommandStat(CommandResultStats.Type type, int amount) {
    }

    public boolean hasPermission(String permission) {
        return this.getPermissionGroups().stream().anyMatch(permissionGroup -> permissionGroup.hasPermission(permission));
    }


    public void setScoreboard(Scoreboard scoreboard) {
        Elytra.getServer().getScoreboardManager().setPlayerScoreboard(this, scoreboard);
    }

    public Scoreboard getScoreboard() {
        return Elytra.getServer().getScoreboardManager().getPlayerScoreboard(this);
    }

    public void updateScoreboard() {
        Elytra.getServer().getScoreboardManager().setPlayerScoreboard(this, getScoreboard());
    }

    public List<PermissionGroup> getPermissionGroups() {
        return ((ElytraServer) Elytra.getServer()).getPermissionManager().getGroups(this);
    }
}

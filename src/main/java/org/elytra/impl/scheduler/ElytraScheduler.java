package org.elytra.impl.scheduler;

import org.elytra.Elytra;
import org.elytra.impl.ElytraServer;

import java.util.concurrent.TimeUnit;

public abstract class ElytraScheduler implements Runnable{

    public void runTaskTimerAsync(long delay, long period, TimeUnit timeUnit){
        ((ElytraServer) Elytra.getServer()).getScheduledExecutorService().scheduleAtFixedRate(this, delay, period, timeUnit);
    }

    public void runTask(){
        ((ElytraServer) Elytra.getServer()).getSchedulerQueue().add(this);
    }


}

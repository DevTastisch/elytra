package org.elytra.impl;

import com.mojang.authlib.GameProfile;
import lombok.Getter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.Packet;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import org.elytra.Main;
import org.elytra.api.player.Player;
import org.elytra.api.Server;
import org.elytra.api.scoreboard.ScoreboardManager;
import org.elytra.impl.config.ConfigManager;
import org.elytra.impl.event.Event;
import org.elytra.impl.event.EventHandler;
import org.elytra.impl.event.Listener;
import org.elytra.impl.permission.PermissionManager;
import org.elytra.impl.player.ElytraPlayer;
import org.elytra.impl.packet.PacketListenerInitializer;
import org.elytra.impl.plugin.ElytraPluginManager;
import org.elytra.impl.scheduler.ElytraScheduler;
import org.elytra.impl.scoreboard.ElytraScoreboardManager;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class ElytraServer implements Server {

    @Getter
    private static ElytraServer instance;
    @Getter
    private Queue<ElytraScheduler> schedulerQueue = new ConcurrentLinkedQueue<>();
    @Getter
    private final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(4);
    private final List<Player> playerList = new LinkedList<>();
    private final List<PacketListenerInitializer<? extends Packet<?>>> globalPacketListenerInitializer = new ArrayList<>();
    private final List<Listener> listeners = new ArrayList<>();
    @Getter
    private final ScoreboardManager scoreboardManager = new ElytraScoreboardManager();
    @Getter
    private final ElytraPluginManager elytraPluginManager;
    @Getter
    private final Logger logger = Logger.getLogger("Minecraft");
    @Getter
    private final PermissionManager permissionManager;
    @Getter
    private final ConfigManager configManager = new ConfigManager();

    public ElytraServer() {
        instance = this;
        this.elytraPluginManager = new ElytraPluginManager(new File("plugins"));
        detectPacketListeners();
        detectInternalListeners();
        MinecraftServer.main(new String[]{"nogui"});
        this.permissionManager = new PermissionManager();
    }

    public MinecraftServer getMinecraftServer() {
        return MinecraftServer.getServer();
    }

    private void detectInternalListeners() {
        synchronized (this.listeners) {
            this.listeners.clear();
            Arrays.stream(new Reflections("", Main.class.getClassLoader(), new SubTypesScanner(false)).getSubTypesOf(Listener.class).toArray(new Class[]{})).forEach(clazz -> {
                try {
                    this.listeners.add((Listener) clazz.newInstance());
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private void detectPacketListeners() {
        synchronized (this.globalPacketListenerInitializer) {
            this.globalPacketListenerInitializer.clear();
            Arrays.stream(new Reflections("org.elytra.impl.packetlistener", Main.class.getClassLoader(), new SubTypesScanner(false)).getSubTypesOf(PacketListenerInitializer.class).toArray(new Class[]{})).forEach(clazz -> {
                try {
                    this.globalPacketListenerInitializer.add((PacketListenerInitializer<?>) clazz.newInstance());
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public Collection<Player> getOnlinePlayers() {
        synchronized (this.playerList) {
            return Collections.unmodifiableCollection(this.playerList);
        }
    }

    public Player createElytraPlayer(EntityPlayer entityPlayer) {
        synchronized (this.playerList) {
            GameProfile gameProfile = entityPlayer.getGameProfile();
            Player player = new ElytraPlayer(entityPlayer);
            Iterator<Player> iterator = this.getOnlinePlayers().stream().filter(search -> search.getUUID().equals(gameProfile.getId())).iterator();
            while (iterator.hasNext()) {
                Player target;
                if ((target = iterator.next()).getUUID().equals(gameProfile.getId())) {
                    ((ElytraPlayer) target).getMP().playerNetServerHandler.getNetworkManager().closeChannel(new ChatComponentText(""));
                    this.playerList.remove(target);
                }
            }
            synchronized (this.globalPacketListenerInitializer) {
                this.globalPacketListenerInitializer.forEach(packetListenerInitializer -> packetListenerInitializer.registerPlayer(player));
            }
            this.playerList.add(player);
            return player;
        }
    }

    public Player getPlayer(UUID uuid) {
        synchronized (this.playerList) {
            return this.playerList.stream().filter(player -> player.getUUID().equals(uuid)).findAny().orElse(null);
        }
    }

    public void callSyncEvent(Event event) {

    }

    public <T extends Event> void callAsyncEvent(T event) {
        synchronized (this.listeners) {
            this.listeners.forEach(listener -> getScheduledExecutorService().execute(() -> Arrays.stream(listener.getClass().getDeclaredMethods())
                    .filter(method -> method.isAnnotationPresent(EventHandler.class))
                    .filter(method -> method.getParameterCount() == 1)
                    .filter(method -> method.getParameterTypes()[0].isInstance(event))
                    .forEach(method -> {
                        method.setAccessible(true);
                        try {
                            method.invoke(listener, event);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    })));
        }
    }

    public <T extends Event> void callAsyncEvent(T event, Consumer<T> consumer) {
        synchronized (this.listeners) {
            getScheduledExecutorService().execute(() -> {
                this.listeners.forEach(listener -> Arrays.stream(listener.getClass().getDeclaredMethods())
                        .filter(method -> method.isAnnotationPresent(EventHandler.class))
                        .filter(method -> method.getParameterCount() == 1)
                        .filter(method -> method.getParameterTypes()[0].isInstance(event))
                        .forEach(method -> {
                            method.setAccessible(true);
                            try {
                                method.invoke(listener, event);
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        }));
                consumer.accept(event);
            });
        }
    }

    public void tick(){
        if(!this.schedulerQueue.isEmpty()) {
            this.schedulerQueue.poll().run();
        }
    }

}

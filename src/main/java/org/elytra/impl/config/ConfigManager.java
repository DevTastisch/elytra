package org.elytra.impl.config;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigManager {

    private Properties properties;
    private File file;

    public ConfigManager() {
        init();
    }

    public void init() {
        this.properties = new Properties();
        this.file = new File("messages.properties");
        try {
            if(!this.file.exists()){
                file.createNewFile();
            }
            FileReader fileReader = new FileReader(file);
            this.properties.load(fileReader);
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public String getValue(String key) {
        return this.properties.getProperty(key);
    }

}

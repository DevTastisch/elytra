package org.elytra.impl.bytecode;

import lombok.RequiredArgsConstructor;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.List;

@RequiredArgsConstructor
public class AnnotationClassTransformer implements ClassFileTransformer {

    private final List<Transformation> transformations;

    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
        System.out.println(classBeingRedefined.getSimpleName());
        return classfileBuffer;
    }
}

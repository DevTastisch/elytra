package org.elytra.impl.plugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;

public class JarClassLoader extends URLClassLoader {

    public JarClassLoader(File file) throws MalformedURLException {
        super(new URL[]{file.toURI().toURL()});
    }

    protected Class<?> findClass(String name) throws ClassNotFoundException {
        return super.findClass(name);
    }

    static {
        ClassLoader.registerAsParallelCapable();
    }

}

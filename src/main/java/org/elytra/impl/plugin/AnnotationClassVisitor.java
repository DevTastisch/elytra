package org.elytra.impl.plugin;

import jdk.internal.org.objectweb.asm.AnnotationVisitor;
import jdk.internal.org.objectweb.asm.ClassVisitor;
import jdk.internal.org.objectweb.asm.Opcodes;
import lombok.Getter;

@Getter
public class AnnotationClassVisitor extends ClassVisitor {

    private final String searchAnnotation;
    private boolean annotationPresent;
    private String className;

    public AnnotationClassVisitor(String searchAnnotation) {
        super(Opcodes.ASM5);
        this.searchAnnotation = searchAnnotation;
    }

    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        className = name.replace('/', '.');
    }

    public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
        if (desc.equals("L" + this.searchAnnotation.replace('.', '/') + ";")) {
            annotationPresent = true;
        }
        return null;
    }

}
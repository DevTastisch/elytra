package org.elytra.impl.plugin;

import jdk.internal.org.objectweb.asm.ClassReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public abstract class AnnotationFinder {

    private final ClassLoader classLoader;
    private final Class clazz;
    private List<String> classNameCache;

    public AnnotationFinder(ClassLoader classLoader, Class clazz) {
        this(new ArrayList<>(), classLoader, clazz);
    }

    public AnnotationFinder(List<String> classNameCache, ClassLoader classLoader, Class clazz) {
        this.classNameCache = classNameCache;
        this.classLoader = classLoader;
        this.clazz = clazz;
        List<URL> result = new ArrayList<>();

        ClassLoader cl = classLoader;
        while (cl != null) {
            if (cl instanceof URLClassLoader) {
                URL[] urls = ((URLClassLoader) cl).getURLs();
                result.addAll(Arrays.asList(urls));
            }
            cl = cl.getParent();
        }
        try {
            for (URL url : result) {
                File f = new File(url.getPath());
                if (f.isDirectory()) {
                    visitFile(f);
                } else {
                    visitJar(url);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void visitFile(File f) throws IOException, ClassNotFoundException {
        if (f.isDirectory()) {
            final File[] children = f.listFiles();
            if (children != null) {
                for (File child : children) {
                    visitFile(child);
                }
            }
        } else if (f.getName().endsWith(".class")) {
            try (FileInputStream in = new FileInputStream(f)) {
                AnnotationClassVisitor cv = new AnnotationClassVisitor(clazz.getName());
                new ClassReader(in).accept(cv, 0);
                if (cv.isAnnotationPresent()) {
                    if (!this.classNameCache.contains(cv.getClassName())) {
                        this.classNameCache.add(cv.getClassName());
                        handleClass(classLoader.loadClass(cv.getClassName()));
                    }
                }
            }
        }
    }

    public void visitJar(URL url) throws IOException, ClassNotFoundException {
        try (InputStream urlIn = url.openStream();
             JarInputStream jarIn = new JarInputStream(urlIn)) {
            JarEntry entry;
            while ((entry = jarIn.getNextJarEntry()) != null) {
                if (entry.getName().endsWith(".class")) {
                    AnnotationClassVisitor cv = new AnnotationClassVisitor(clazz.getName());
                    new ClassReader(jarIn).accept(cv, 0);
                    if (cv.isAnnotationPresent()) {
                        if (!this.classNameCache.contains(cv.getClassName())) {
                            this.classNameCache.add(cv.getClassName());
                            handleClass(classLoader.loadClass(cv.getClassName()));
                        }
                    }
                }
            }
        }
    }

    public abstract void handleClass(Class clazz);

}

package org.elytra.impl.plugin;

import lombok.Getter;
import org.elytra.Elytra;
import org.elytra.api.command.Command;
import org.elytra.api.plugin.Plugin;
import org.elytra.api.property.plugin.PluginMain;
import org.elytra.api.property.plugin.PluginName;
import org.elytra.impl.ElytraServer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ElytraPlugin implements Plugin {

    private static List<ElytraPlugin> plugins = new ArrayList<>();
    private File file;
    @Getter
    private JarClassLoader jarClassLoader;

    public static List<ElytraPlugin> getPlugins() {
        return plugins;
    }

    public void load(File file){
        this.file = file;
        try {
            this.jarClassLoader = new JarClassLoader(file);
            Enumeration<JarEntry> enumeration = new JarFile(file).entries();
            Class<? extends ElytraPlugin> mainClass = null;
            while (enumeration.hasMoreElements()) {
                JarEntry jarEntry = enumeration.nextElement();
                if (!(jarEntry.isDirectory() || !jarEntry.getName().endsWith(".class"))) {
                    String className = jarEntry.getName().substring(0, jarEntry.getName().length() - 6).replaceAll("/", ".");
                    Class<?> aClass = jarClassLoader.loadClass(className.replaceAll("/", "."));
                    if (ElytraPlugin.class.isAssignableFrom(aClass) &&
                            aClass.isAnnotationPresent(PluginMain.class) &&
                            aClass.isAnnotationPresent(PluginName.class)){
                        if (mainClass != null) {
                            //TODO Error
                        } else {
                            mainClass = ((Class<? extends ElytraPlugin>) aClass);
                        }
                    }
                }
            }
            PluginName pluginName = mainClass.getAnnotation(PluginName.class);
            ElytraPlugin elytraPlugin = mainClass.newInstance();
            elytraPlugin.onEnable();
            System.out.println("Found Plugin " + pluginName.value());
            plugins.add(this);
        } catch (ClassNotFoundException | IOException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void onEnable() {
    }

    public void onDisable() {
    }

    public boolean isLoaded() {
        return false;
    }
}

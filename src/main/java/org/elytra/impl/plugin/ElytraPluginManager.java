package org.elytra.impl.plugin;

import org.elytra.api.plugin.Plugin;
import org.elytra.api.plugin.PluginManager;

import java.io.File;

public class ElytraPluginManager implements PluginManager {

    private final File pluginsDir;

    public ElytraPluginManager(File pluginsDir){
        this.pluginsDir = pluginsDir;
        if(!this.pluginsDir.exists()){
            this.pluginsDir.mkdirs();
        }
        this.detectPlugins();
    }

    public void loadPlugin(Plugin plugin) {

    }

    public void unloadPlugin(Plugin plugin) {

    }

    public Plugin getPlugin(String plugin) {
        return null;
    }

    public void detectPlugins() {
        for (File file : this.pluginsDir.listFiles()) {
            new ElytraPlugin().load(file);
        }
    }
}

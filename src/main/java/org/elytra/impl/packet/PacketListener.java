package org.elytra.impl.packet;

import lombok.Getter;
import net.minecraft.network.EnumPacketDirection;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;

@Getter
public abstract class PacketListener<T extends Packet<?>> {

    private final NetworkManager networkManager;
    private final EnumPacketDirection enumPacketDirection;
    private final TypeMatcher<T> typeMatcher;

    public PacketListener(NetworkManager networkManager, EnumPacketDirection enumPacketDirection) {
        this.networkManager = networkManager;
        this.enumPacketDirection = enumPacketDirection;
        this.typeMatcher = new TypeMatcher<>();
    }


    public void fire0(Packet packet) {
        this.typeMatcher.executeWhenMatch(packet, this::fire);
    }

    public abstract void fire(T packet);

    public void unregister() {
        this.networkManager.unregisterPacketListener(this);
    }

}

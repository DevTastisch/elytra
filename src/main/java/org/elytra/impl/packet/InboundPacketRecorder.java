package org.elytra.impl.packet;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.RequiredArgsConstructor;
import net.minecraft.network.EnumPacketDirection;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;

@RequiredArgsConstructor
public class InboundPacketRecorder extends ChannelInboundHandlerAdapter {

    private final NetworkManager networkManager;

    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        super.channelRead(ctx, msg);
        synchronized (this.networkManager.getPacketListeners()) {
            this.networkManager.getPacketListeners().stream().filter(packetListener -> packetListener.getEnumPacketDirection() == EnumPacketDirection.SERVERBOUND).forEach(packetListener -> packetListener.fire0(((Packet) msg)));
        }
    }
}

package org.elytra.impl.packet;

import lombok.RequiredArgsConstructor;
import net.minecraft.network.EnumPacketDirection;
import net.minecraft.network.Packet;
import org.elytra.api.player.Player;

import java.util.function.BiConsumer;

@RequiredArgsConstructor
public abstract class PacketListenerInitializer<T extends Packet<?>> {

    private final EnumPacketDirection enumPacketDirection;


    public abstract void handle(Player player, T packet);

    public void registerPlayer(Player player){
        if(this.enumPacketDirection.equals(EnumPacketDirection.CLIENTBOUND)){
            player.addOutgoingPacketListener((BiConsumer<Player, T>)(p, packet)-> handle(player, packet));
        }else if(this.enumPacketDirection.equals(EnumPacketDirection.SERVERBOUND)){
            player.addIncomingPacketListener((BiConsumer<Player, T>)(p, packet)-> handle(player, packet));
        }
    }
}

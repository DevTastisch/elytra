package org.elytra.impl.packet;

import java.util.function.Consumer;

public class TypeMatcher<T> {

    public void executeWhenMatch(Object object, Consumer<T> consumer){
        try {
            consumer.accept(((T) object));
        }catch (ClassCastException ex){
        }
    }

}

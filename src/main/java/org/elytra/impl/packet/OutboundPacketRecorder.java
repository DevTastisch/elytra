package org.elytra.impl.packet;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import lombok.RequiredArgsConstructor;
import net.minecraft.network.EnumPacketDirection;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;

@RequiredArgsConstructor
public class OutboundPacketRecorder extends ChannelOutboundHandlerAdapter {

    private final NetworkManager networkManager;

    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        super.write(ctx, msg, promise);
        this.networkManager.getPacketListeners().stream().filter(packetListener -> packetListener.getEnumPacketDirection() == EnumPacketDirection.CLIENTBOUND).forEach(packetListener -> packetListener.fire0(((Packet) msg)));
    }
}

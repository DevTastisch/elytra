package org.elytra.impl.listener;

import com.google.common.base.Objects;
import net.minecraft.entity.Entity;
import net.minecraft.network.play.client.C01PacketChatMessage;
import net.minecraft.util.BlockPos;
import net.minecraft.world.ChunkCoordIntPair;
import net.minecraft.world.WorldServer;
import org.elytra.Elytra;
import org.elytra.api.player.Player;
import org.elytra.api.world.Block;
import org.elytra.api.world.Chunk;
import org.elytra.impl.ElytraServer;
import org.elytra.impl.event.EventHandler;
import org.elytra.impl.event.Listener;
import org.elytra.impl.event.player.AsyncPlayerJoinEvent;
import org.elytra.impl.event.player.AsyncPlayerToggleSneakEvent;
import org.elytra.impl.event.player.AsyncPlayerToggleSprintEvent;
import org.elytra.impl.player.ElytraPlayer;
import org.elytra.impl.world.ElytraBlock;
import org.elytra.impl.world.ElytraChunk;
import org.elytra.impl.world.ElytraWorld;
import org.elytra.impl.world.Location;

import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;

public class TestListener implements Listener {


    @EventHandler
    public void handle(AsyncPlayerJoinEvent e){
        e.setJoinMessage(null);
    }



    /*
    Async Place blocks and stuff like that +-+
     */
//    @EventHandler
//    public void handle(AsyncPlayerToggleSprintEvent event) {
//        ((ElytraServer) Elytra.getServer()).getScheduledExecutorService().execute(() -> {
//            if (event.isSprinting()) return;
//            Set<BlockPos> chunkCoordIntPairList = new HashSet<>();
//            for (int i = 0; i < 30; i++) {
//                for (int j = 0; j < 30; j++) {
//                    for (int l = 0; l < 30; l++) {
//                        final int jj = j;
//                        final int ii = i;
//                        final int ll = l;
//                        chunkCoordIntPairList.add(new BlockPos(event.getPlayer().getLocation().getX() + jj, event.getPlayer().getLocation().getY() + ii, event.getPlayer().getLocation().getZ() + ll));
//                        event.getPlayer().getWorld().getBlockAt(event.getPlayer().getLocation().getX() + jj, event.getPlayer().getLocation().getY() + ii, event.getPlayer().getLocation().getZ() + ll).setTypeId(1);
//                    }
//                }
//            }
//            for (BlockPos pos : chunkCoordIntPairList) {
//                ((WorldServer) ((ElytraWorld) event.getPlayer().getWorld()).getNmsWorld()).getPlayerManager().markBlockForUpdate(pos);
//            }
//        });
//    }

}

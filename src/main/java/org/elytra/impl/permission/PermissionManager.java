package org.elytra.impl.permission;

import lombok.Getter;
import org.elytra.api.permission.PermissionGroup;
import org.elytra.api.player.Player;
import org.sqlite.SQLiteException;

import java.io.File;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class PermissionManager {

    @Getter
    private List<PermissionGroup> permissionGroups = new ArrayList<>();
    @Getter
    private PermissionConnection permissionConnection = new PermissionConnection(new File("./permissions.db"));

    public PermissionManager() {
        createDefaultTables(() -> {
            loadGroups();
        });
    }

    public void createDefaultTables(Runnable runnable) {
        this.permissionConnection.executeUpdate("CREATE TABLE IF NOT EXISTS groups ( id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL, prefix VARCHAR NOT NULL, suffix VARCHAR NOT NULL, priority INTEGER )", () -> {
            this.permissionConnection.executeUpdate("CREATE TABLE IF NOT EXISTS group_permissions (id INTEGER PRIMARY KEY, group_id INTEGER, permission VARCHAR NOT NULL)", () -> {
                runnable.run();
            });
        });
    }

    public void loadGroups() {
        this.permissionConnection.executeQuery("SELECT * FROM groups", (resultSet) -> {
            try {
                this.permissionGroups.clear();
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String name = resultSet.getString("name");
                    String prefix = resultSet.getString("prefix");
                    String suffix = resultSet.getString("suffix");
                    int priority = resultSet.getInt("priority");
                    this.permissionGroups.add(new ElytraPermissionGroup(id, name, prefix, suffix, priority));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
    }

    public PermissionGroup getGroup(String name){
        for (PermissionGroup permissionGroup : this.permissionGroups) {
            if(permissionGroup.getName().equalsIgnoreCase(name)){
                return permissionGroup;
            }
        }
        return null;
    }

    public List<PermissionGroup> getGroups(Player player){
        return this.permissionGroups.stream().filter(permissionGroup -> permissionGroup.getMembers().contains(player)).collect(Collectors.toList());
    }

    public void createGroup(String name, String prefix, String suffix, int priority) {
        this.permissionConnection.executeUpdate("INSERT INTO groups (name, prefix, suffix, priority) VALUES('" + name + "', '" + prefix + "', '" + suffix + "', '" + priority + "'", () -> {
            loadGroups();
        });
    }


}

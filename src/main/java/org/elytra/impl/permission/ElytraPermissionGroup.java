package org.elytra.impl.permission;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.elytra.Elytra;
import org.elytra.api.permission.PermissionGroup;
import org.elytra.api.player.Player;
import org.elytra.impl.ElytraServer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
public class ElytraPermissionGroup implements PermissionGroup {

    private final int id;
    private String name;
    private String prefix;
    private String suffix;
    private int priority;
    private List<PermissionGroup> inheritedGroups = new ArrayList<>();
    private List<String> permissions = new ArrayList<>();
    private Set<Player> members = new HashSet<>();

    public ElytraPermissionGroup(int id, String name, String prefix, String suffix, int priority) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.suffix = suffix;
        this.priority = priority;
        this.loadPermissions();
    }

    public void loadPermissions() {
        ((ElytraServer) Elytra.getServer()).getPermissionManager().getPermissionConnection().executeQuery("SELECT * FROM group_permissions WHERE group_id = " + this.id, (resultSet) -> {
            try {
                this.permissions.clear();
                while (resultSet.next()) {
                    this.permissions.add(resultSet.getString("permission"));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
    }

    public boolean hasPermission(String permission) {
        return this.permissions.contains(permission);
    }

    public void setSuffix(String suffix) {

    }

    public void setPrefix(String prefix) {

    }

    public void addPermission(String permission) {
        ((ElytraServer) Elytra.getServer()).getPermissionManager().getPermissionConnection().executeUpdate("INSERT INTO group_permissions(group_id, permission) SELECT " + this.getId() + ", '" + permission + "' WHERE NOT EXISTS (SELECT * FROM group_permissions WHERE permission='" + permission + "' AND group_id='" + this.getId() + "')", this::loadPermissions);
    }

    public void removePermission(String permission) {
        ((ElytraServer) Elytra.getServer()).getPermissionManager().getPermissionConnection().executeUpdate("DELETE FROM group_permissions WHERE permission='" + permission + "' AND group_id='" + this.getId() + "'", this::loadPermissions);
    }
}

package org.elytra.impl.permission;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.elytra.Elytra;
import org.elytra.impl.ElytraServer;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Consumer;

@RequiredArgsConstructor
@Getter
public class PermissionConnection {

    private final File file;
    private Connection connection;

    public void connect(){
        try {
            if(connection == null || !connection.isClosed()){
                if(!this.file.exists()){
                    this.file.createNewFile();
                }
                connection = DriverManager.getConnection("jdbc:sqlite:" + file.getPath());
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    public void executeQuery(String query, Consumer<ResultSet> consumer){
        ((ElytraServer) Elytra.getServer()).getScheduledExecutorService().execute(() -> {
            connect();
            try {
                consumer.accept(this.connection.prepareStatement(query).executeQuery());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void executeUpdate(String update, Consumer<Integer> consumer){
        ((ElytraServer) Elytra.getServer()).getScheduledExecutorService().execute(() -> {
            connect();
            try {
                consumer.accept(this.connection.prepareStatement(update).executeUpdate());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void executeUpdate(String update, Runnable runnable){
        ((ElytraServer) Elytra.getServer()).getScheduledExecutorService().execute(() -> {
            connect();
            try {
                this.connection.prepareStatement(update).executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            runnable.run();
        });
    }

    public void executeQuery(String query, Runnable runnable){
        ((ElytraServer) Elytra.getServer()).getScheduledExecutorService().execute(() -> {
            connect();
            try {
                this.connection.prepareStatement(query).executeQuery();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            runnable.run();
        });
    }

}

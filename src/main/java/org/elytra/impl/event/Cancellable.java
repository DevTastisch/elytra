package org.elytra.impl.event;

public interface Cancellable {

    boolean isCancelled();

    void setCancelled(boolean cancelled);

}

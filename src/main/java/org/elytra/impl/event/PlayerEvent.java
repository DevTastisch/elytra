package org.elytra.impl.event;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.elytra.api.player.Player;

@RequiredArgsConstructor
@Getter
public class PlayerEvent extends Event {

    private final Player player;

}

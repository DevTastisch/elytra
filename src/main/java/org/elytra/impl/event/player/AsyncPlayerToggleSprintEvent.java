package org.elytra.impl.event.player;

import lombok.Data;
import org.elytra.api.player.Player;
import org.elytra.impl.event.PlayerEvent;

@Data
public class AsyncPlayerToggleSprintEvent extends PlayerEvent {

    private boolean sprinting;
    public AsyncPlayerToggleSprintEvent(Player player, boolean sprinting){
        super(player);
        this.sprinting = sprinting;
    }

}

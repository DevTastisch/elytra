package org.elytra.impl.event.player;

import lombok.Setter;
import org.elytra.api.player.Player;
import org.elytra.impl.event.PlayerEvent;

public class AsyncPlayerJoinEvent extends PlayerEvent {

    @Setter
    private String joinMessage;


    public AsyncPlayerJoinEvent(Player player, String joinMessage){
        super(player);
        this.joinMessage = joinMessage;
    }

    public String getJoinMessage(Object... params){
        String formattedMessage = this.joinMessage;
        for (int i = 0; i < params.length; i++) {
            formattedMessage = formattedMessage.replaceAll("\\{" + i + "}", params[i].toString());
            System.out.println(params[i].toString());
        }
        return formattedMessage;
    }

}

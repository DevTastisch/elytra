package org.elytra.impl.event.player;

import lombok.Data;
import org.elytra.Elytra;
import org.elytra.api.permission.PermissionGroup;
import org.elytra.api.player.Player;
import org.elytra.impl.ElytraServer;
import org.elytra.impl.event.Cancellable;
import org.elytra.impl.event.PlayerEvent;
import org.elytra.impl.scoreboard.DefaultScoreboardSidebarHandler;
import org.elytra.impl.scoreboard.DisplaySlot;

@Data
public class AsyncPlayerToggleSneakEvent extends PlayerEvent implements Cancellable {

    private final boolean sneaking;
    private boolean cancelled;

    public AsyncPlayerToggleSneakEvent(Player player, boolean sneaking) {
        super(player);
        this.sneaking = sneaking;
        for (PermissionGroup permissionGroup : ElytraServer.getInstance().getPermissionManager().getPermissionGroups()) {
            permissionGroup.getMembers().add(player);
        }
        player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setScoreboardHandler(new DefaultScoreboardSidebarHandler());
        ElytraServer.getInstance().getScoreboardManager().updatePermissionTeams(player,  player.getScoreboard());
        player.updateScoreboard();
    }

}
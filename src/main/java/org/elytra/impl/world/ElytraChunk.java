package org.elytra.impl.world;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.util.BlockPos;
import net.minecraft.world.WorldServer;
import org.elytra.Elytra;
import org.elytra.api.world.Block;
import org.elytra.api.world.Chunk;
import org.elytra.api.world.World;
import org.elytra.impl.ElytraServer;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

@RequiredArgsConstructor
public class ElytraChunk implements Chunk {

    @Getter
    private final net.minecraft.world.chunk.Chunk chunk;
    private final Map<String, Set<ChunkExecution<?>>> executions = new HashMap<>();

    public Block getBlockAt(int x, int y, int z) {
        return new ElytraBlock(new BlockPos(this.getX() * 16 + x, y, this.getZ() * 16 + z), this, x, y, z);
    }

    public Block getBlockAt(Location location) {
        return this.getBlockAt(location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    public int getTypeId(BlockPos blockposition) {
        return net.minecraft.block.Block.getIdFromBlock(this.chunk.getBlock(blockposition));
    }

    public World getWorld() {
        return chunk.getWorld().getNethWorld();
    }

    public final <T> void execute(String key, BiConsumer<ElytraChunk, T[]> consumer, T... properties) {
        Set<ChunkExecution<?>> chunkExecutions;
        this.executions.computeIfAbsent(key, k -> new HashSet<>());

        chunkExecutions = this.executions.get(key);
        boolean execute = false;
        if (!chunkExecutions.isEmpty()) {
            execute = true;
        }
        chunkExecutions.add(new ChunkExecution<T>(key, this, consumer, properties));
        if (execute) {
            chunkExecutions.stream().findFirst().get().execute();
        }
    }


    public int getX() {
        return this.chunk.xPosition;
    }

    public int getZ() {
        return this.chunk.zPosition;
    }

    @AllArgsConstructor
    static class ChunkExecution<T> {

        private final String key;
        private final ElytraChunk elytraChunk;
        private final BiConsumer<ElytraChunk, T[]> biConsumer;
        private final T[] properties;

        public void execute() {
            ((ElytraServer) Elytra.getServer()).getScheduledExecutorService().execute(() -> {
                elytraChunk.executions.get(key).remove(this);
                biConsumer.andThen((elytraChunk, properties) -> {
                    elytraChunk.executions.get("update").stream().findFirst().ifPresent(ChunkExecution::execute);
                }).accept(elytraChunk, properties);
            });
        }
    }
}

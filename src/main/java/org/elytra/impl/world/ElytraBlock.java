package org.elytra.impl.world;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.block.BlockFire;
import net.minecraft.block.BlockGrass;
import net.minecraft.block.BlockStone;
import net.minecraft.block.BlockTallGrass;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.BlockPos;
import org.elytra.Elytra;
import org.elytra.api.world.Block;
import org.elytra.api.world.Chunk;
import org.elytra.api.world.World;
import org.elytra.impl.ElytraServer;

import java.util.concurrent.locks.ReentrantLock;

import static net.minecraft.block.BlockFire.FLIP;

@Getter
@AllArgsConstructor
public class ElytraBlock implements Block {

    private final BlockPos blockPos;
    private final Chunk chunk;
    private final int xrel;
    private final int yrel;
    private final int zrel;

    public void setTypeId(int type) {
        ((ElytraServer) Elytra.getServer()).getScheduledExecutorService().execute(() -> {
            ((ElytraWorld) this.chunk.getWorld()).getNmsWorld().setBlockState(blockPos, net.minecraft.block.Block.blockRegistry.getObjectById(type).getDefaultState());
        });
    }

    public int getTypeId() {
        return chunk.getTypeId(new BlockPos(xrel, yrel, zrel));
    }

    public net.minecraft.block.Block getNMSBlock(){
        return ((ElytraChunk) chunk).getChunk().getBlock(xrel, yrel, zrel);
    }

    public World getWorld() {
        return this.chunk.getWorld();
    }

    public int getX() {
        return this.blockPos.getX();
    }

    public int getY() {
        return this.blockPos.getY();
    }

    public int getZ() {
        return this.blockPos.getZ();
    }

    public byte getLightLevel() {
        return this.chunk.getBlockAt(getX(), getY(), getZ()).getLightLevel();
    }
}

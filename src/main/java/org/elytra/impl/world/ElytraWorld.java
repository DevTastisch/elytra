package org.elytra.impl.world;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.util.BlockPos;
import org.elytra.api.world.Block;
import org.elytra.api.world.Chunk;
import org.elytra.api.world.World;

import java.util.UUID;

@RequiredArgsConstructor
public class ElytraWorld implements World {

    @Getter
    private final net.minecraft.world.World nmsWorld;

    public String getName() {
        return this.nmsWorld.getWorldInfo().getWorldName();
    }

    public UUID getUUID() {
        return this.nmsWorld.getWorldInfo().getUuid();
    }

    public Chunk getChunkAt(int x, int z) {
        return this.nmsWorld.getChunkProvider().provideChunk(x, z).getElytraChunk();
    }

    public Chunk getChunkAt(Location location) {
        return getChunkAt(location.getBlockX() >> 4, location.getBlockZ() >> 4);
    }

    public Block getBlockAt(double x, double y, double z) {
        Chunk chunkAt = getChunkAt(((int) x) >> 4, ((int) z) >> 4);
        return chunkAt.getBlockAt((int) (((((int) x) / 16D) - chunkAt.getX()) * 16), ((int) y), (int) (((((int) z) / 16D) - chunkAt.getZ()) * 16));
    }

    public Block getBlockAt(Location location) {
        return getBlockAt(location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    public boolean isChunkLoaded(int x, int z) {
        return getNmsWorld().getChunkProvider().chunkExists(x, z);
    }

    public boolean refreshChunk(int x, int z) {
        if (!isChunkLoaded(x, z)) {
            return false;
        }

        int px = x << 4;
        int pz = z << 4;

        // If there are more than 64 updates to a chunk at once, it will update all 'touched' sections within the chunk
        // And will include biome data if all sections have been 'touched'
        // This flags 65 blocks distributed across all the sections of the chunk, so that everything is sent, including biomes
        int height = 256 / 16;
        for (int idx = 0; idx < 64; idx++) {
            nmsWorld.notifyLightSet(new BlockPos(px + (idx / height), ((idx % height) * 16), pz));
        }
        getNmsWorld().notifyLightSet(new BlockPos(px + 15, (height * 16) - 1, pz + 15));

        return true;
    }
}

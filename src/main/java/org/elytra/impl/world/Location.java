package org.elytra.impl.world;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.minecraft.util.BlockPos;
import org.elytra.api.world.Block;
import org.elytra.api.world.Chunk;
import org.elytra.api.world.World;

@Data
@AllArgsConstructor
public class Location {

    private World world;
    private double x;
    private double y;
    private double z;
    private float yaw;
    private float pitch;

    public Location(World world, double x, double y, double z){
        this(world, x, y, z, 0, 0);
    }

    public Location(World world, BlockPos blockPos, float yaw, float pitch){
        this.world = world;
        this.x = blockPos.getX();
        this.y = blockPos.getY();
        this.z = blockPos.getZ();
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public BlockPos toBlockPos() {
        return new BlockPos(this.x, this.y, this.z);
    }

    public int getBlockX() {
        return ((int) this.x);
    }

    public int getBlockY() {
        return ((int) this.y);
    }

    public int getBlockZ() {
        return ((int) this.z);
    }

    public Block getBlock(){
        return this.world.getBlockAt(this);
    }

    public Chunk getChunk(){
        return this.getWorld().getChunkAt(this);
    }

}

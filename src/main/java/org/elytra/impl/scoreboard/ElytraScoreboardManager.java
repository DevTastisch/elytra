package org.elytra.impl.scoreboard;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S3BPacketScoreboardObjective;
import net.minecraft.network.play.server.S3CPacketUpdateScore;
import net.minecraft.network.play.server.S3DPacketDisplayScoreboard;
import net.minecraft.network.play.server.S3EPacketTeams;
import net.minecraft.scoreboard.ScoreObjective;
import net.minecraft.scoreboard.ScorePlayerTeam;
import org.elytra.Elytra;
import org.elytra.api.permission.PermissionGroup;
import org.elytra.api.player.Player;
import org.elytra.api.scoreboard.*;
import org.elytra.api.scoreboard.handler.ScoreboardSidebarHandler;
import org.elytra.impl.ElytraServer;

import java.util.*;

public class ElytraScoreboardManager implements ScoreboardManager {

    private final Map<String, Scoreboard> scoreboards = new HashMap<>();
    private final Map<Player, Scoreboard> playerBoards = new HashMap<>();
    private Scoreboard mainScoreboard;

    public Scoreboard getScoreboard(String name) {
        synchronized (this.scoreboards) {
            if (!this.scoreboards.containsKey(name)) {
                this.scoreboards.put(name, new ElytraScoreboard(new net.minecraft.scoreboard.Scoreboard()));
            }
            return this.scoreboards.get(name);
        }
    }

    public Scoreboard getMainScoreboard() {
        if (this.mainScoreboard == null) {
            this.mainScoreboard = new ElytraScoreboard(new net.minecraft.scoreboard.Scoreboard());
            this.mainScoreboard.getObjective(DisplaySlot.SIDEBAR).setScoreboardHandler(new DefaultScoreboardSidebarHandler());
        }
        return this.mainScoreboard;
    }

    public void updatePermissionTeams(Player player, Scoreboard scoreboard) {
        synchronized (this.playerBoards){
            for (PermissionGroup permissionGroup : ElytraServer.getInstance().getPermissionManager().getPermissionGroups()) {
                if(!scoreboard.hasTeam("group" + permissionGroup.getName())){
                    Team team = scoreboard.getTeam("group" + permissionGroup.getName());
                    for (Player player1 : permissionGroup.getMembers()) {
                        if(!team.getEntries().contains(player1.getName())){
                            team.addEntry(player1.getName());
                        }
                    }
                    team.setSuffix(permissionGroup.getSuffix());
                    team.setPrefix(permissionGroup.getPrefix());
                }else{
                    Team team = scoreboard.getTeam("group" + permissionGroup.getName());
                    for (Player player1 : permissionGroup.getMembers()) {
                        if(!team.getEntries().contains(player1.getName())){
                            team.addEntry(player1.getName());
                        }
                    }
                    team.setSuffix(permissionGroup.getSuffix());
                    team.setPrefix(permissionGroup.getPrefix());
                }
            }
        }
    }

    public void setPlayerScoreboard(Player player, Scoreboard scoreboard) {
        synchronized (this.playerBoards) {
            if (scoreboard == mainScoreboard) {
                this.playerBoards.remove(player);
            } else {
                this.playerBoards.put(player, scoreboard);
            }

            Objective objective = scoreboard.getObjective(DisplaySlot.SIDEBAR);
            Object cachedObject = ((ElytraObjective) objective).getCachedObject();

            ScoreboardSidebarHandler scoreboardSidebarHandler = (ScoreboardSidebarHandler) ((ElytraObjective) scoreboard.getObjective(DisplaySlot.SIDEBAR)).getScoreboardHandler();
            String[] lines = (scoreboardSidebarHandler).getLines().toArray(new String[]{});
            objective.setDisplayName(scoreboardSidebarHandler.getTitle());

            if (cachedObject instanceof String[]) {
                String[] cachedLines = (String[]) cachedObject;
                if (cachedLines.length != lines.length) {
                    objective.resetScores();
                    for (int i = 0; i < Math.max(cachedLines.length, lines.length); i++) {
                        Team team = scoreboard.getTeam("t" + i);
                        team.unregister();
                        player.sendPacket(new S3EPacketTeams(((ElytraTeam) team).getTeam(), 1));
                    }
                }
            }

            for (int i = 0; i < lines.length; i++) {
                objective.getScore("§" + i).setValue(lines.length - i);
                boolean create = !scoreboard.hasTeam("t" + i);
                Team team = scoreboard.getTeam("t" + i);
                team.setSuffix("§f" + lines[i]);
                team.addEntry("§" + i);
                if (create) {
                    player.sendPacket(new S3EPacketTeams(((ElytraTeam) team).getTeam(), 0));
                } else {
                    player.sendPacket(new S3EPacketTeams(((ElytraTeam) team).getTeam(), 2));
                }
            }



            net.minecraft.scoreboard.Scoreboard oldboard = ((ElytraScoreboard) getPlayerScoreboard(player)).getScoreboard();

            HashSet<ScoreObjective> removed = new HashSet<ScoreObjective>();
            ScoreObjective scoreboardobjective = oldboard.getObjectiveInDisplaySlot(1);
            if (scoreboardobjective != null && !removed.contains(scoreboardobjective)) {
                player.sendPacket(new S3BPacketScoreboardObjective(scoreboardobjective, 1));
                removed.add(scoreboardobjective);
            }

            ((ElytraObjective) objective).setCachedObject(lines);


            if (scoreboardobjective != null) {
                List list = getScoreboardScorePacketsForObjective(((ElytraScoreboard) scoreboard).getScoreboard(), scoreboardobjective);
                Iterator iterator1 = list.iterator();

                while (iterator1.hasNext()) {
                    Packet packet = (Packet) iterator1.next();
                    player.sendPacket(packet);
                }
            }
        }
    }


    public Scoreboard getPlayerScoreboard(Player player) {
        synchronized (this.playerBoards) {
            return this.playerBoards.containsKey(player) ? this.playerBoards.get(player) : getMainScoreboard();
        }
    }

    public List<Packet> getScoreboardScorePacketsForObjective(net.minecraft.scoreboard.Scoreboard scoreboard, ScoreObjective scoreObjective) {
        ArrayList arraylist = Lists.newArrayList();

        arraylist.add(new S3BPacketScoreboardObjective(scoreObjective, 0));
        arraylist.add(new S3DPacketDisplayScoreboard(1, scoreObjective));


        Iterator iterator = scoreboard.getSortedScores(scoreObjective).iterator();

        while (iterator.hasNext()) {
            net.minecraft.scoreboard.Score scoreboardscore = (net.minecraft.scoreboard.Score) iterator.next();

            arraylist.add(new S3CPacketUpdateScore(scoreboardscore));
        }

        return arraylist;
    }

}

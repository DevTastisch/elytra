package org.elytra.impl.scoreboard;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import lombok.Data;
import lombok.Getter;
import net.minecraft.network.play.server.S3EPacketTeams;
import net.minecraft.scoreboard.IScoreObjectiveCriteria;
import net.minecraft.scoreboard.ScoreObjective;
import net.minecraft.scoreboard.ScorePlayerTeam;
import org.elytra.Elytra;
import org.elytra.api.player.Player;
import org.elytra.api.scoreboard.Objective;
import org.elytra.api.scoreboard.Score;
import org.elytra.api.scoreboard.Scoreboard;
import org.elytra.api.scoreboard.ScoreboardHandler;
import org.elytra.api.scoreboard.handler.ScoreboardSidebarHandler;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

@Data
public class ElytraObjective implements Objective {

    private final Scoreboard scoreboard;
    private final ScoreObjective scoreObjective;
    private final DisplaySlot displaySlot;
    private Object cachedObject;
    private ScoreboardHandler scoreboardHandler;

    public ElytraObjective(Scoreboard scoreboard, DisplaySlot displaySlot) {
        this.scoreboard = scoreboard;
        this.scoreObjective = ((ElytraScoreboard) this.scoreboard).getScoreboard().addScoreObjective(displaySlot.name(), ElytraCriteria.getDummy().getCriteria());
        this.displaySlot = displaySlot;
        ((ElytraScoreboard) this.scoreboard).getScoreboard().setObjectiveInDisplaySlot(displaySlot.getSlotId(), this.scoreObjective);

    }

    public void setDisplayName(String displayName) {
        this.scoreObjective.setDisplayName(displayName);
    }

    public DisplaySlot getDisplaySlot() {
        //TODO
        return null;
    }

    public Score getScore(String key) {
        return new ElytraScore(this, key);
    }

    public String getDisplayName() {
        return this.scoreObjective.getDisplayName();
    }

    public void resetScores() {
        System.out.println("Reset");
        for (String s : getEntries()) {
            for (ScoreObjective objective : ((ElytraScoreboard) this.scoreboard).getScoreboard().getScoreObjectives()) {
                ((ElytraScoreboard) scoreboard).getScoreboard().removeObjectiveFromEntity(s, objective);
            }
        }
    }

    public ImmutableSet<String> getEntries() {
        ImmutableSet.Builder<String> entries = ImmutableSet.builder();
        for (Object entry : ((ElytraScoreboard) scoreboard).getScoreboard().getObjectiveNames()) {
            entries.add(entry.toString());
        }
        return entries.build();
    }
}

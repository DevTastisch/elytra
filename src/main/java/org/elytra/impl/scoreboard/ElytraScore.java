package org.elytra.impl.scoreboard;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.elytra.api.scoreboard.Objective;
import org.elytra.api.scoreboard.Score;

@Getter
@RequiredArgsConstructor
public class ElytraScore implements Score {

    private final Objective objective;
    private final String key;

    public int getValue() {
        return ((ElytraScoreboard) this.objective.getScoreboard()).getScoreboard().getValueFromObjective(key, ((ElytraObjective) objective).getScoreObjective()).getScorePoints();
    }

    public void setValue(int value) {
        ((ElytraScoreboard) this.objective.getScoreboard()).getScoreboard().getValueFromObjective(key, ((ElytraObjective) objective).getScoreObjective()).setScorePoints(value);
    }

}

package org.elytra.impl.scoreboard;

import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import net.minecraft.scoreboard.IScoreObjectiveCriteria;
import net.minecraft.scoreboard.ScoreObjective;

import java.util.Map;

@Getter
public class ElytraCriteria {


    private static Map<String, ElytraCriteria> defaults;
    @Getter
    private static ElytraCriteria dummy;

    static {
        ImmutableMap.Builder<String, ElytraCriteria> defaults = ImmutableMap.builder();

        for (Map.Entry<?, ?> entry : ((Map<?,?> ) IScoreObjectiveCriteria.INSTANCES).entrySet()) {
            String name = entry.getKey().toString();
            IScoreObjectiveCriteria criteria = (IScoreObjectiveCriteria) entry.getValue();

            defaults.put(name, new ElytraCriteria(criteria));
        }

        ElytraCriteria.defaults = defaults.build();
        ElytraCriteria.dummy = ElytraCriteria.defaults.get("dummy");

    }

    private final IScoreObjectiveCriteria criteria;
    private final String name;

    private ElytraCriteria(String name){
        this.name = name;
        this.criteria = dummy.criteria;
    }

    private ElytraCriteria(IScoreObjectiveCriteria criteria){
        this.criteria = criteria;
        this.name = criteria.getName();
    }

    public static ElytraCriteria getFromNMS(ScoreObjective objective){
        return defaults.get(objective.getCriteria().getName());
    }

    public static ElytraCriteria getFromElytra(String name){
        final ElytraCriteria criteria = defaults.get(name);
        if(criteria != null){
            return criteria;
        }

        return new ElytraCriteria(name);
    }

    public boolean equals(Object that) {
        if (!(that instanceof ElytraCriteria)) {
            return false;
        }
        return ((ElytraCriteria) that).name.equals(this.name);
    }

    public int hashCode() {
        return this.name.hashCode() ^ ElytraCriteria.class.hashCode();
    }


}

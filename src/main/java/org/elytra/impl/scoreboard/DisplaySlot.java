package org.elytra.impl.scoreboard;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum DisplaySlot {

    SIDEBAR(1);

    private final int slotId;

}

package org.elytra.impl.scoreboard;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.scoreboard.ScorePlayerTeam;
import org.elytra.api.scoreboard.Objective;
import org.elytra.api.scoreboard.Scoreboard;
import org.elytra.api.scoreboard.Team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class ElytraScoreboard implements Scoreboard {

    private final net.minecraft.scoreboard.Scoreboard scoreboard;
    private Map<DisplaySlot, Objective> objectives = new HashMap<>();

    public ElytraScoreboard(net.minecraft.scoreboard.Scoreboard scoreboard){
        this.scoreboard = scoreboard;
        this.scoreboard.getScoreObjectives().forEach(objective -> objective.getCriteria().getName());
    }

    public Objective getObjective(DisplaySlot displaySlot) {
        if(!objectives.containsKey(displaySlot)){
            objectives.put(displaySlot, new ElytraObjective(this, displaySlot));
        }
        return objectives.get(displaySlot);
    }

    public Team getTeam(String name) {
        if(this.hasTeam(name)){
            return new ElytraTeam(this, this.scoreboard.getTeam(name));
        }
        return this.createTeam(name);
    }

    public boolean hasTeam(String name){
        return this.scoreboard.getTeam(name) != null;
    }

    public Team createTeam(String name){
        return new ElytraTeam(this, this.scoreboard.createTeam(name));
    }
}

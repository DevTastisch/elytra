package org.elytra.impl.scoreboard;

import org.elytra.api.scoreboard.handler.ScoreboardSidebarHandler;

import java.util.Arrays;
import java.util.List;

public class DefaultScoreboardSidebarHandler implements ScoreboardSidebarHandler {

    public String getTitle() {
        return "§e§lElytra";
    }

    public List<String> getLines() {
        return Arrays.asList("Hello", "World!", "§k-wdawdawd");
    }
}

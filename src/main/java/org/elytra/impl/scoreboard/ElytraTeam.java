package org.elytra.impl.scoreboard;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.elytra.api.scoreboard.Scoreboard;
import org.elytra.api.scoreboard.Team;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Getter
public class ElytraTeam implements Team {

    private final Scoreboard scoreboard;
    private final net.minecraft.scoreboard.ScorePlayerTeam team;

    public String getName() {
        return this.team.getTeamName();
    }

    public String getPrefix() {
        return this.team.getColorPrefix();
    }

    public String getSuffix() {
        return this.team.getColorSuffix();
    }

    public void setPrefix(String prefix) {
        this.team.setNamePrefix(prefix);
    }

    public void setSuffix(String suffix) {
        this.team.setNameSuffix(suffix);
    }

    public void addEntry(String entry) {
        ((ElytraScoreboard) this.scoreboard).getScoreboard().addPlayerToTeam(entry, team.getRegisteredName());
    }

    public void removeEntry(String entry) {
        this.team.getMembershipCollection().remove(entry);
    }

    public void clearEntries() {
        this.team.getMembershipCollection().clear();
    }

    public List<String> getEntries() {
        return Collections.unmodifiableList(new ArrayList<>(this.team.getMembershipCollection()));
    }

    public void unregister(){
        ((ElytraScoreboard) this.scoreboard).getScoreboard().removeTeam(team);
    }
}

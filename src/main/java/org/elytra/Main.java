package org.elytra;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.bytecode.*;
import javassist.bytecode.annotation.ArrayMemberValue;
import javassist.bytecode.annotation.MemberValue;
import javassist.bytecode.annotation.StringMemberValue;
import net.bytebuddy.agent.ByteBuddyAgent;
import org.elytra.api.property.general.ConfigLink;
import org.elytra.impl.ElytraServer;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;

import java.io.ByteArrayInputStream;
import java.lang.annotation.Annotation;
import java.lang.instrument.Instrumentation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        premain(null, ByteBuddyAgent.install());
        new ElytraServer();
    }

    public static void premain(String args, Instrumentation instrumentation) {
        instrumentation.addTransformer((loader, className, classBeingRedefined, protectionDomain, classfileBuffer) -> {
            try {

                ClassPool classPool = ClassPool.getDefault();
                CtClass ctClass = classPool.makeClass(new ByteArrayInputStream(classfileBuffer));

                if (className != null) {
                    if (ctClass.hasAnnotation(ConfigLink.class)) {
                        ConfigLink annotation = ((ConfigLink) ctClass.getAnnotation(ConfigLink.class));
                        for (Class<? extends Annotation> aClass : annotation.value()) {
                            Method method = aClass.getDeclaredMethod("value");
                            Map<String, MemberValue> memberValueMap = new HashMap<>();
                            ctClass.defrost();
                            if (ctClass.hasAnnotation(aClass)) {
                                if (method.getReturnType().isArray()) {
                                    ArrayMemberValue arrayMemberValue = new ArrayMemberValue(ctClass.getClassFile().getConstPool());
                                    String[] strings = (String[]) method.invoke(ctClass.getAnnotation(aClass));
                                    StringMemberValue[] stringMemberValue = new StringMemberValue[strings.length];
                                    for (int i = 0; i < strings.length; i++) {
                                        stringMemberValue[i] = new StringMemberValue(ctClass.getClassFile().getConstPool());
                                        stringMemberValue[i].setValue(((ElytraServer) Elytra.getServer()).getConfigManager().getValue(strings[i]));
                                    }
                                    arrayMemberValue.setValue(stringMemberValue);
                                    memberValueMap.put("value", arrayMemberValue);
                                } else {
                                    StringMemberValue stringMemberValue = new StringMemberValue(ctClass.getClassFile().getConstPool());
                                    String invoke = (String) method.invoke(ctClass.getAnnotation(aClass));
                                    stringMemberValue.setValue(ElytraServer.getInstance().getConfigManager().getValue(invoke));
                                    memberValueMap.put("value", stringMemberValue);
                                }
                                removeAnnotationFromClass(ctClass, aClass);
                                addAnnotationToClass(ctClass, aClass, memberValueMap);
                            }
                        }
                    }
                    for (CtField ctField : ctClass.getDeclaredFields()) {
                        if (ctField.hasAnnotation(ConfigLink.class)) {
                            ConfigLink configLink = (ConfigLink) ctField.getAnnotation(ConfigLink.class);
                            if (!configLink.key().isEmpty()) {
                                String modifier = Modifier.toString(ctField.getFieldInfo().getAccessFlags());
                                CtField make = CtField.make(modifier + " " + ctField.getType().getName() + " " + ctField.getName() + " = \"" + ElytraServer.getInstance().getConfigManager().getValue(configLink.key()) + "\";", ctClass);
                                ctClass.removeField(ctField);
                                ctClass.addField(make);
                            }
                        }
                    }



                }

                byte[] bytes = ctClass.toBytecode();
                ctClass.detach();
                return bytes;

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return classfileBuffer;
        });
    }


    public static void removeAnnotationFromClass(CtClass clazz, Class<? extends Annotation> annotationClass) throws Exception {
        AnnotationsAttribute attr = (AnnotationsAttribute) clazz.getClassFile().getAttribute(AnnotationsAttribute.visibleTag);
        attr.removeAnnotation(annotationClass.getName());
    }
    public static void removeAnnotationFromField(CtClass clazz, String fieldName, Class<? extends Annotation> annotationClass) throws Exception {
        AnnotationsAttribute attr = (AnnotationsAttribute) clazz.getDeclaredField(fieldName).getFieldInfo().getAttribute(AnnotationsAttribute.visibleTag);
        attr.removeAnnotation(annotationClass.getName());
    }

    public static void addAnnotationToClass(CtClass clazz, Class<? extends Annotation> annotationClass, Map<String, MemberValue> members) throws Exception {
        ClassFile cfile = clazz.getClassFile();
        ConstPool cpool = cfile.getConstPool();
        AnnotationsAttribute attr = new AnnotationsAttribute(cpool, AnnotationsAttribute.visibleTag);
        javassist.bytecode.annotation.Annotation annot = new javassist.bytecode.annotation.Annotation(annotationClass.getName(), cpool);
        members.forEach(annot::addMemberValue);
        attr.addAnnotation(annot);
        clazz.getClassFile().addAttribute(attr);
    }

}

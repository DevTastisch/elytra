package org.elytra.api.scoreboard;

import org.elytra.impl.scoreboard.DisplaySlot;

import java.util.List;

public interface Scoreboard {

    Objective getObjective(DisplaySlot displaySlot);

    Team getTeam(String name);

    Team createTeam(String name);

    boolean hasTeam(String name);

}

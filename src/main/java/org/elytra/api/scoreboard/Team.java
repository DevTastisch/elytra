package org.elytra.api.scoreboard;

import java.util.List;

public interface Team {

    String getName();

    String getPrefix();

    String getSuffix();

    void setPrefix(String prefix);

    void setSuffix(String suffix);

    void addEntry(String entry);

    void removeEntry(String entry);

    void clearEntries();

    List<String> getEntries();

    void unregister();

}

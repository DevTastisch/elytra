package org.elytra.api.scoreboard.handler;

import org.elytra.api.scoreboard.ScoreboardHandler;

import java.util.List;

public interface ScoreboardSidebarHandler extends ScoreboardHandler {

    String getTitle();

    List<String> getLines();

}

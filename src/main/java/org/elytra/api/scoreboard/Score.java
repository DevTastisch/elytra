package org.elytra.api.scoreboard;

public interface Score {

    Objective getObjective();

    String getKey();

    int getValue();

    void setValue(int value);

}

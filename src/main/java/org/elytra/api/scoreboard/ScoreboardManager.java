package org.elytra.api.scoreboard;

import org.elytra.api.player.Player;

import java.util.List;

public interface ScoreboardManager {

    Scoreboard getScoreboard(String name);

    Scoreboard getMainScoreboard();

    Scoreboard getPlayerScoreboard(Player player);

    void setPlayerScoreboard(Player player, Scoreboard scoreboard);

    void updatePermissionTeams(Player player, Scoreboard scoreboard);

}

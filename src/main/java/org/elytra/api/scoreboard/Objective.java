package org.elytra.api.scoreboard;

import com.google.common.collect.ImmutableSet;
import org.elytra.impl.scoreboard.DisplaySlot;

public interface Objective {

    Scoreboard getScoreboard();

    DisplaySlot getDisplaySlot();

    void setDisplayName(String displayName);

    String getDisplayName();

    Score getScore(String key);

    void resetScores();

    void setScoreboardHandler(ScoreboardHandler scoreboardHandler);

    ImmutableSet<String> getEntries();
}

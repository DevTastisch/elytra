package org.elytra.api.command;

import org.elytra.api.player.Player;

public interface Command {

    void exceute(Player player, String[] args);

}
package org.elytra.api.command.annotation;

public interface IsUsernameIndex {

    boolean isUsernameIndex(String[] args, int index);

}

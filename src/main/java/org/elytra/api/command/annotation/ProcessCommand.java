package org.elytra.api.command.annotation;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;

public interface ProcessCommand {
    void processCommand(ICommandSender sender, String[] args) throws CommandException;
}

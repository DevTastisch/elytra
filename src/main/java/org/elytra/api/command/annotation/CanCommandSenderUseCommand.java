package org.elytra.api.command.annotation;

import net.minecraft.command.ICommandSender;

public interface CanCommandSenderUseCommand {

    boolean canCommandSenderUseCommand(ICommandSender sender);

}

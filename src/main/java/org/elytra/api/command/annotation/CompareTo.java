package org.elytra.api.command.annotation;

import net.minecraft.command.ICommand;

public interface CompareTo {

    int compareTo(ICommand o);

}

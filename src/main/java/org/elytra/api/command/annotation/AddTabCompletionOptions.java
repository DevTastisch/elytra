package org.elytra.api.command.annotation;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.BlockPos;

import java.util.List;

public interface AddTabCompletionOptions {
    List<String> addTabCompletionOptions(ICommandSender sender, String[] args, BlockPos pos);
}

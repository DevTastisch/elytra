package org.elytra.api;

import net.minecraft.entity.player.EntityPlayer;
import org.elytra.api.player.Player;
import org.elytra.api.scoreboard.Scoreboard;
import org.elytra.api.scoreboard.ScoreboardManager;
import org.elytra.impl.event.Event;
import org.elytra.impl.ElytraServer;

import java.util.Collection;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.logging.Logger;

public interface Server {

    Player getPlayer(UUID uuid);

    Player createElytraPlayer(EntityPlayer entityPlayer);

    Collection<Player> getOnlinePlayers();

    void callSyncEvent(Event event);

    <T extends Event> void callAsyncEvent(T event, Consumer<T> consumer);

    <T extends Event> void callAsyncEvent(T event);


    Logger getLogger();

    ScoreboardManager getScoreboardManager();


    static Server getServer() {
        return ElytraServer.getInstance();
    }
}

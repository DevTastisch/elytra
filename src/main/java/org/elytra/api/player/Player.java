package org.elytra.api.player;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.Packet;
import net.minecraft.util.IChatComponent;
import org.elytra.api.permission.PermissionGroup;
import org.elytra.api.scoreboard.Scoreboard;
import org.elytra.api.world.World;
import org.elytra.impl.world.Location;

import java.util.List;
import java.util.UUID;
import java.util.function.BiConsumer;

public interface Player extends ICommandSender {

    Location getLocation();

    boolean hasPermission(String permission);

    Player sendMessage(String message);

    Player sendMessage(IChatComponent message);

    <T extends Packet<?>> Player addIncomingPacketListener(BiConsumer<Player, T> consumer);

    <T extends Packet<?>> Player addOutgoingPacketListener(BiConsumer<Player, T> consumer);

    Player sendPacket(Packet<?> packet);

    UUID getUUID();

    boolean isSprinting();

    boolean isSneaking();

    EntityPlayerMP getMP();

    World getWorld();

    void setScoreboard(Scoreboard scoreboard);

    Scoreboard getScoreboard();

    void updateScoreboard();

    List<PermissionGroup> getPermissionGroups();

}

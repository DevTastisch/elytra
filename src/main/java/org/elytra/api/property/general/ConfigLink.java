package org.elytra.api.property.general;

import org.elytra.Main;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.FIELD, ElementType.LOCAL_VARIABLE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigLink {

    Class<? extends Annotation>[] value() default {};
    String key() default "";


}

package org.elytra.api.property.command;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface CommandName {

    String value() default "";


}

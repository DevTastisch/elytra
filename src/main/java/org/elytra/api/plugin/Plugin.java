package org.elytra.api.plugin;

public interface Plugin {

    void onEnable();

    void onDisable();

    boolean isLoaded();

}

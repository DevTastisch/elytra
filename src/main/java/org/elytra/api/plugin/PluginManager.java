package org.elytra.api.plugin;

public interface PluginManager {

    void loadPlugin(Plugin plugin);

    void unloadPlugin(Plugin plugin);

    Plugin getPlugin(String plugin);

    void detectPlugins();

}

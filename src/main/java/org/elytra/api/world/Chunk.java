package org.elytra.api.world;

import net.minecraft.util.BlockPos;
import org.elytra.impl.world.Location;

public interface Chunk {

    Block getBlockAt(int x, int y, int z);

    Block getBlockAt(Location location);

    int getX();

    int getZ();

    int getTypeId(final BlockPos blockposition);

    World getWorld();

}

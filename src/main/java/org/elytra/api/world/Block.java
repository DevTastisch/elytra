package org.elytra.api.world;

public interface Block {

    void setTypeId(int type);

    int getTypeId();

    World getWorld();

    int getX();
    int getY();
    int getZ();

    byte getLightLevel();

}

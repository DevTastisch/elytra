package org.elytra.api.world;

import org.elytra.impl.world.Location;

import java.util.UUID;

public interface World {

    String getName();

    UUID getUUID();

    Block getBlockAt(double x, double y, double z);

    Block getBlockAt(Location location);

    Chunk getChunkAt(int x, int z);

    Chunk getChunkAt(Location location);

}

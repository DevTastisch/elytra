package org.elytra.api.permission;

import org.elytra.api.player.Player;

import java.util.List;
import java.util.Set;

public interface PermissionGroup {

    String getName();

    void setSuffix(String suffix);

    void setPrefix(String prefix);

    String getSuffix();

    String getPrefix();

    int getPriority();

    List<PermissionGroup> getInheritedGroups();

    List<String> getPermissions();

    Set<Player> getMembers();

    boolean hasPermission(String permission);

    void addPermission(String permission);

    void removePermission(String permission);

}

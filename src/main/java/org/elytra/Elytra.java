package org.elytra;

import net.minecraft.util.IChatComponent;
import org.elytra.api.Server;
import org.elytra.api.player.Player;

import java.util.Collection;
import java.util.Objects;

public class Elytra {

    public static void broadcastMessage(String message) {
        getOnlinePlayers().forEach(player -> player.sendMessage(message));
        System.out.println(message);
    }

    public static void broadcastMessage(IChatComponent message) {
        getOnlinePlayers().forEach(player -> player.sendMessage(message));
        System.out.println(message);
    }



    public static void broadcastMessage(Object object){
        broadcastMessage(object.toString());
    }

    public static Collection<Player> getOnlinePlayers() {
        return getServer().getOnlinePlayers();
    }

    public static Server getServer() {
        return Server.getServer();
    }

}
